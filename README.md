Formanek CI Pipeline
====================

This project aims to create a pipeline that can be used for continuous integration.

Currently, the services are launched on different ports.
My personal aim is to run them on different virtual hosts using Jason Wilder's nginx-proxy.

Installation
------------

After cloning the repository, you have to execute `./download_java.sh` to download binary files that are not included in the repository.
Then, you can run the thing with a simple `docker-compose up`.

