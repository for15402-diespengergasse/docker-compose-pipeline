#!/bin/sh
cd jenkins && echo "download_java.sh[1]: Entering directory jenkins"
echo wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u92-b14/jdk-8u92-linux-x64.tar.gz
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u92-b14/jdk-8u92-linux-x64.tar.gz
echo wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war
wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war
cd .. && echo "download_java.sh[1]: Leaving directory jenkins"

